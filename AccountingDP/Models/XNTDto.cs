﻿using System;

namespace AccountingDP.Models
{
    public class XNTDto
    {
        public string? Makhohang { get; set; }
        public string? Mavtu { get; set; }
        public string? Tenvtu { get; set; }
        public string? Donvile { get; set; }
        public decimal? Toncuoikysl { get; set; }

        public decimal? Toncuoikygt { get; set; }
        public decimal Giavon
        {
            get
            {
                if (Toncuoikysl == 0) return 0;
                return (decimal)(Toncuoikygt / Toncuoikysl);
            }
        }

        public decimal? GiaBanChuaVat
        {
            get
            {
                return Math.Round(Giavon * System.Convert.ToDecimal(1.1), 0);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;

namespace AccountingDP.EF.Models.VietseaModel;

public partial class Vattugd
{
    public string Mavattugdpk { get; set; } = null!;

    public string? Madonvi { get; set; }

    public string? Maptnx { get; set; }

    public string? Soctugoc { get; set; }

    public DateTime? Ngayctgoc { get; set; }

    public DateTime? Ngayhachtoan { get; set; }

    public string? Ghichu { get; set; }

    public string? Manvlapbieu { get; set; }

    public decimal? Trangthai { get; set; }

    public string? Soctkt { get; set; }

    public string? Trangthaihd { get; set; }

    public string? Hovatengiaodich { get; set; }

    public string? Diachi { get; set; }

    public string? Kemtheo { get; set; }

    public string Manhomptnx { get; set; } = null!;

    public string? Mactphanbocp { get; set; }

    public decimal? Ttmactphanbocp { get; set; }

    public decimal? Loaiphanbocp { get; set; }

    public string? Mactphanbolephi { get; set; }

    public decimal? Tienmactphanbolephi { get; set; }

    public decimal? Loaiphanbolephi { get; set; }

    public string? Mactphanbocpkhac { get; set; }

    public decimal? Tienmactphanbocpkhac { get; set; }

    public decimal? Loaiphanbocpkhac { get; set; }

    public string? Congthucgiavon { get; set; }

    public decimal? Songaythanhtoan { get; set; }

    public string? Mactthanhtoan { get; set; }

    public decimal? Tiendathanhtoan { get; set; }

    public string? Makhachang { get; set; }

    public string? Mahopdong { get; set; }

    public string? Masothue { get; set; }

    public DateTime? Ngaythanhtoan { get; set; }

    public decimal? Tylelai { get; set; }

    public DateTime? Hantra { get; set; }

    public decimal? Sumtienvat { get; set; }

    public decimal? Sumtienvatnk { get; set; }

    public decimal? Sumtienvatdb { get; set; }

    public string? Manhomkh { get; set; }

    public string? Manv { get; set; }

    public decimal? Dieuchuyen { get; set; }

    public string? Nvdathang { get; set; }

    public string? Mactkttienthuong { get; set; }

    public decimal? Gopdon { get; set; }

    public string? Shoppingid { get; set; }

    public string? Mactktpk { get; set; }

    public string? Guidkey { get; set; }

    public DateTime? Ngaydenhanthanhtoan { get; set; }

    public decimal? Trangthaihanhtoan { get; set; }

    public decimal? Sumtienhang { get; set; }

    public decimal? Sumtienck { get; set; }

    public decimal? Sumtiengg { get; set; }

    public decimal? Sumtienkm { get; set; }

    public decimal? Sumtiencp { get; set; }

    public decimal? Sumtiencpkhac { get; set; }

    public decimal? Sumtienlephi { get; set; }

    public decimal? Sumtiencksaubh { get; set; }

    public decimal? Sumsoluong { get; set; }

    public decimal? Sumthanhtien { get; set; }

    public decimal? Sumtienthanhtoan { get; set; }

    public DateTime? Ngayhoadongd { get; set; }

    public string? Sohoadongd { get; set; }

    public string? Kyhieuhoadongd { get; set; }

    public string? Tenkhachhang { get; set; }

    public string? Masothuegd { get; set; }

    public decimal? Vatgd { get; set; }

    public string? Matkthuevatnogd { get; set; }

    public string? Matkthuevatcogd { get; set; }

    public string? Mavatgd { get; set; }

    public string? Loaictgd { get; set; }

    public string? Mathanggd { get; set; }

    public decimal? Dsovatgd { get; set; }

    public decimal? Dsochuavatgd { get; set; }

    public DateTime? Ngaytao { get; set; }

    public DateTime? Ngaysua { get; set; }

    public string? Usertao { get; set; }

    public string? Usersua { get; set; }

    public string? Manhacungcap { get; set; }

    public decimal? Sort { get; set; }

    public string? Madonvinhap1 { get; set; }
}

﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace AccountingDP.EF.Models.VietseaModel;

public partial class VietseaContext : DbContext
{
    public VietseaContext()
    {
    }

    public VietseaContext(DbContextOptions<VietseaContext> options)
        : base(options)
    {
    }

    public virtual DbSet<Vattugd> Vattugds { get; set; }

    public virtual DbSet<Vattugdct> Vattugdcts { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
        => optionsBuilder.UseOracle("Data Source=192.168.0.50:1522/TBNETERP;User Id=TBNETERP;Password=TBNETERP");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.HasDefaultSchema("TBNETERP");

        modelBuilder.Entity<Vattugd>(entity =>
        {
            entity.HasKey(e => e.Mavattugdpk).HasName("VATTUGD_PK");

            entity.ToTable("VATTUGD");

            entity.HasIndex(e => new { e.Ngayhachtoan, e.Manhomptnx, e.Trangthai, e.Madonvi }, "IDX$$_00010014");

            entity.HasIndex(e => e.Ngayhachtoan, "IDX$$_00010016");

            entity.HasIndex(e => new { e.Maptnx, e.Ngayhachtoan, e.Mavattugdpk }, "IDX$$_0001002");

            entity.HasIndex(e => new { e.Manhomptnx, e.Trangthai, e.Madonvi }, "IDX$$_0001003");

            entity.Property(e => e.Mavattugdpk)
                .HasMaxLength(50)
                .HasColumnName("MAVATTUGDPK");
            entity.Property(e => e.Congthucgiavon)
                .HasMaxLength(500)
                .HasColumnName("CONGTHUCGIAVON");
            entity.Property(e => e.Diachi)
                .HasMaxLength(500)
                .HasColumnName("DIACHI");
            entity.Property(e => e.Dieuchuyen)
                .HasDefaultValueSql("0")
                .HasColumnType("NUMBER")
                .HasColumnName("DIEUCHUYEN");
            entity.Property(e => e.Dsochuavatgd)
                .HasDefaultValueSql("0")
                .HasColumnType("NUMBER(38,17)")
                .HasColumnName("DSOCHUAVATGD");
            entity.Property(e => e.Dsovatgd)
                .HasDefaultValueSql("0")
                .HasColumnType("NUMBER(38,17)")
                .HasColumnName("DSOVATGD");
            entity.Property(e => e.Ghichu)
                .HasMaxLength(500)
                .HasColumnName("GHICHU");
            entity.Property(e => e.Gopdon)
                .HasDefaultValueSql("0")
                .HasColumnType("NUMBER")
                .HasColumnName("GOPDON");
            entity.Property(e => e.Guidkey)
                .HasDefaultValueSql("0")
                .HasColumnType("NVARCHAR2(4000)")
                .HasColumnName("GUIDKEY");
            entity.Property(e => e.Hantra)
                .HasColumnType("DATE")
                .HasColumnName("HANTRA");
            entity.Property(e => e.Hovatengiaodich)
                .HasMaxLength(500)
                .HasColumnName("HOVATENGIAODICH");
            entity.Property(e => e.Kemtheo)
                .HasMaxLength(500)
                .HasColumnName("KEMTHEO");
            entity.Property(e => e.Kyhieuhoadongd)
                .HasMaxLength(50)
                .HasColumnName("KYHIEUHOADONGD");
            entity.Property(e => e.Loaictgd)
                .HasMaxLength(50)
                .HasColumnName("LOAICTGD");
            entity.Property(e => e.Loaiphanbocp)
                .HasColumnType("NUMBER")
                .HasColumnName("LOAIPHANBOCP");
            entity.Property(e => e.Loaiphanbocpkhac)
                .HasColumnType("NUMBER")
                .HasColumnName("LOAIPHANBOCPKHAC");
            entity.Property(e => e.Loaiphanbolephi)
                .HasColumnType("NUMBER")
                .HasColumnName("LOAIPHANBOLEPHI");
            entity.Property(e => e.Mactktpk)
                .HasColumnType("NVARCHAR2(4000)")
                .HasColumnName("MACTKTPK");
            entity.Property(e => e.Mactkttienthuong)
                .HasMaxLength(50)
                .HasColumnName("MACTKTTIENTHUONG");
            entity.Property(e => e.Mactphanbocp)
                .HasMaxLength(500)
                .HasColumnName("MACTPHANBOCP");
            entity.Property(e => e.Mactphanbocpkhac)
                .HasMaxLength(500)
                .HasColumnName("MACTPHANBOCPKHAC");
            entity.Property(e => e.Mactphanbolephi)
                .HasMaxLength(500)
                .HasColumnName("MACTPHANBOLEPHI");
            entity.Property(e => e.Mactthanhtoan)
                .HasMaxLength(500)
                .HasColumnName("MACTTHANHTOAN");
            entity.Property(e => e.Madonvi)
                .HasMaxLength(20)
                .IsUnicode(false)
                .HasColumnName("MADONVI");
            entity.Property(e => e.Madonvinhap1)
                .HasMaxLength(100)
                .HasColumnName("MADONVINHAP1");
            entity.Property(e => e.Mahopdong)
                .HasMaxLength(50)
                .HasColumnName("MAHOPDONG");
            entity.Property(e => e.Makhachang)
                .HasMaxLength(50)
                .IsUnicode(false)
                .HasColumnName("MAKHACHANG");
            entity.Property(e => e.Manhacungcap)
                .HasMaxLength(100)
                .HasColumnName("MANHACUNGCAP");
            entity.Property(e => e.Manhomkh)
                .HasMaxLength(1000)
                .IsUnicode(false)
                .HasColumnName("MANHOMKH");
            entity.Property(e => e.Manhomptnx)
                .HasMaxLength(50)
                .HasColumnName("MANHOMPTNX");
            entity.Property(e => e.Manv)
                .HasMaxLength(50)
                .IsUnicode(false)
                .HasColumnName("MANV");
            entity.Property(e => e.Manvlapbieu)
                .HasMaxLength(20)
                .HasColumnName("MANVLAPBIEU");
            entity.Property(e => e.Maptnx)
                .HasMaxLength(50)
                .HasColumnName("MAPTNX");
            entity.Property(e => e.Masothue)
                .HasMaxLength(50)
                .HasColumnName("MASOTHUE");
            entity.Property(e => e.Masothuegd)
                .HasMaxLength(50)
                .HasColumnName("MASOTHUEGD");
            entity.Property(e => e.Mathanggd)
                .HasMaxLength(200)
                .HasColumnName("MATHANGGD");
            entity.Property(e => e.Matkthuevatcogd)
                .HasMaxLength(50)
                .HasColumnName("MATKTHUEVATCOGD");
            entity.Property(e => e.Matkthuevatnogd)
                .HasMaxLength(50)
                .HasColumnName("MATKTHUEVATNOGD");
            entity.Property(e => e.Mavatgd)
                .HasMaxLength(50)
                .HasColumnName("MAVATGD");
            entity.Property(e => e.Ngayctgoc)
                .HasColumnType("DATE")
                .HasColumnName("NGAYCTGOC");
            entity.Property(e => e.Ngaydenhanthanhtoan)
                .HasColumnType("DATE")
                .HasColumnName("NGAYDENHANTHANHTOAN");
            entity.Property(e => e.Ngayhachtoan)
                .HasColumnType("DATE")
                .HasColumnName("NGAYHACHTOAN");
            entity.Property(e => e.Ngayhoadongd)
                .HasColumnType("DATE")
                .HasColumnName("NGAYHOADONGD");
            entity.Property(e => e.Ngaysua)
                .HasDefaultValueSql("SYSDATE")
                .HasColumnType("DATE")
                .HasColumnName("NGAYSUA");
            entity.Property(e => e.Ngaytao)
                .HasDefaultValueSql("SYSDATE")
                .HasColumnType("DATE")
                .HasColumnName("NGAYTAO");
            entity.Property(e => e.Ngaythanhtoan)
                .HasColumnType("DATE")
                .HasColumnName("NGAYTHANHTOAN");
            entity.Property(e => e.Nvdathang)
                .HasMaxLength(50)
                .IsUnicode(false)
                .HasColumnName("NVDATHANG");
            entity.Property(e => e.Shoppingid)
                .HasMaxLength(500)
                .HasColumnName("SHOPPINGID");
            entity.Property(e => e.Soctkt)
                .HasMaxLength(50)
                .HasColumnName("SOCTKT");
            entity.Property(e => e.Soctugoc)
                .HasMaxLength(50)
                .HasColumnName("SOCTUGOC");
            entity.Property(e => e.Sohoadongd)
                .HasMaxLength(50)
                .HasColumnName("SOHOADONGD");
            entity.Property(e => e.Songaythanhtoan)
                .HasColumnType("NUMBER")
                .HasColumnName("SONGAYTHANHTOAN");
            entity.Property(e => e.Sort)
                .HasDefaultValueSql("0")
                .HasColumnType("NUMBER(38,17)")
                .HasColumnName("SORT");
            entity.Property(e => e.Sumsoluong)
                .HasDefaultValueSql("0")
                .HasColumnType("NUMBER(38,17)")
                .HasColumnName("SUMSOLUONG");
            entity.Property(e => e.Sumthanhtien)
                .HasDefaultValueSql("0")
                .HasColumnType("NUMBER(38,17)")
                .HasColumnName("SUMTHANHTIEN");
            entity.Property(e => e.Sumtienck)
                .HasDefaultValueSql("0")
                .HasColumnType("NUMBER(38,17)")
                .HasColumnName("SUMTIENCK");
            entity.Property(e => e.Sumtiencksaubh)
                .HasDefaultValueSql("0")
                .HasColumnType("NUMBER(38,17)")
                .HasColumnName("SUMTIENCKSAUBH");
            entity.Property(e => e.Sumtiencp)
                .HasDefaultValueSql("0")
                .HasColumnType("NUMBER(38,17)")
                .HasColumnName("SUMTIENCP");
            entity.Property(e => e.Sumtiencpkhac)
                .HasDefaultValueSql("0")
                .HasColumnType("NUMBER(38,17)")
                .HasColumnName("SUMTIENCPKHAC");
            entity.Property(e => e.Sumtiengg)
                .HasDefaultValueSql("0")
                .HasColumnType("NUMBER(38,17)")
                .HasColumnName("SUMTIENGG");
            entity.Property(e => e.Sumtienhang)
                .HasDefaultValueSql("0")
                .HasColumnType("NUMBER(38,17)")
                .HasColumnName("SUMTIENHANG");
            entity.Property(e => e.Sumtienkm)
                .HasDefaultValueSql("0")
                .HasColumnType("NUMBER(38,17)")
                .HasColumnName("SUMTIENKM");
            entity.Property(e => e.Sumtienlephi)
                .HasDefaultValueSql("0")
                .HasColumnType("NUMBER(38,17)")
                .HasColumnName("SUMTIENLEPHI");
            entity.Property(e => e.Sumtienthanhtoan)
                .HasDefaultValueSql("0")
                .HasColumnType("NUMBER(38,17)")
                .HasColumnName("SUMTIENTHANHTOAN");
            entity.Property(e => e.Sumtienvat)
                .HasDefaultValueSql("0")
                .HasColumnType("NUMBER(38,17)")
                .HasColumnName("SUMTIENVAT");
            entity.Property(e => e.Sumtienvatdb)
                .HasDefaultValueSql("0")
                .HasColumnType("NUMBER(38,17)")
                .HasColumnName("SUMTIENVATDB");
            entity.Property(e => e.Sumtienvatnk)
                .HasDefaultValueSql("0")
                .HasColumnType("NUMBER(38,17)")
                .HasColumnName("SUMTIENVATNK");
            entity.Property(e => e.Tenkhachhang)
                .HasMaxLength(200)
                .HasColumnName("TENKHACHHANG");
            entity.Property(e => e.Tiendathanhtoan)
                .HasDefaultValueSql("0")
                .HasColumnType("NUMBER(38,17)")
                .HasColumnName("TIENDATHANHTOAN");
            entity.Property(e => e.Tienmactphanbocpkhac)
                .HasColumnType("NUMBER")
                .HasColumnName("TIENMACTPHANBOCPKHAC");
            entity.Property(e => e.Tienmactphanbolephi)
                .HasColumnType("NUMBER")
                .HasColumnName("TIENMACTPHANBOLEPHI");
            entity.Property(e => e.Trangthai)
                .HasColumnType("NUMBER(38,4)")
                .HasColumnName("TRANGTHAI");
            entity.Property(e => e.Trangthaihanhtoan)
                .HasDefaultValueSql("0")
                .HasColumnType("NUMBER(38,17)")
                .HasColumnName("TRANGTHAIHANHTOAN");
            entity.Property(e => e.Trangthaihd)
                .HasMaxLength(20)
                .IsUnicode(false)
                .HasColumnName("TRANGTHAIHD");
            entity.Property(e => e.Ttmactphanbocp)
                .HasColumnType("NUMBER")
                .HasColumnName("TTMACTPHANBOCP");
            entity.Property(e => e.Tylelai)
                .HasDefaultValueSql("0")
                .HasColumnType("NUMBER(38,17)")
                .HasColumnName("TYLELAI");
            entity.Property(e => e.Usersua)
                .HasMaxLength(100)
                .HasColumnName("USERSUA");
            entity.Property(e => e.Usertao)
                .HasMaxLength(100)
                .HasColumnName("USERTAO");
            entity.Property(e => e.Vatgd)
                .HasDefaultValueSql("0")
                .HasColumnType("NUMBER")
                .HasColumnName("VATGD");
        });

        modelBuilder.Entity<Vattugdct>(entity =>
        {
            entity.HasKey(e => e.Itemid);

            entity.ToTable("VATTUGDCT");

            entity.HasIndex(e => new { e.Manhomvtu, e.Mavattugdpk }, "IDX$$_00010010");

            entity.HasIndex(e => new { e.Mavattugdpk, e.Hangkhuyenmai, e.Madonvi }, "IDX$$_00010019");

            entity.HasIndex(e => new { e.Madonvi, e.Mavattugdpk, e.Hangkhuyenmai, e.Mavtu, e.Makhoxuat }, "IDX$$_00010020");

            entity.HasIndex(e => e.Mavattugdpk, "IDX$$_0001004");

            entity.HasIndex(e => new { e.Mavattugdpk, e.Madonvi }, "IDX$$_0001005");

            entity.HasIndex(e => new { e.Maloaivtu, e.Mavattugdpk }, "IDX$$_0001006");

            entity.HasIndex(e => e.Tienhang, "IDX$$_0001007");

            entity.HasIndex(e => e.Maloaivtu, "IDX$$_0001008");

            entity.HasIndex(e => e.Manhomvtu, "IDX$$_0001009");

            entity.Property(e => e.Itemid)
                .ValueGeneratedOnAdd()
                .HasDefaultValueSql("NULL")
                .HasColumnType("NUMBER")
                .HasColumnName("ITEMID");
            entity.Property(e => e.Barcodevtu)
                .HasMaxLength(50)
                .ValueGeneratedOnAdd()
                .HasColumnName("BARCODEVTU");
            entity.Property(e => e.Cao)
                .ValueGeneratedOnAdd()
                .HasDefaultValueSql("0")
                .HasColumnType("NUMBER(38,17)")
                .HasColumnName("CAO");
            entity.Property(e => e.Ctdoituong)
                .ValueGeneratedOnAdd()
                .HasColumnType("NVARCHAR2(4000)")
                .HasColumnName("CTDOITUONG");
            entity.Property(e => e.Ctdoituongco)
                .HasMaxLength(200)
                .ValueGeneratedOnAdd()
                .HasColumnName("CTDOITUONGCO");
            entity.Property(e => e.Ctdoituongcp)
                .ValueGeneratedOnAdd()
                .HasColumnType("NVARCHAR2(4000)")
                .HasColumnName("CTDOITUONGCP");
            entity.Property(e => e.Ctdoituongkm)
                .ValueGeneratedOnAdd()
                .HasColumnType("NVARCHAR2(4000)")
                .HasColumnName("CTDOITUONGKM");
            entity.Property(e => e.Ctdoituongno)
                .HasMaxLength(200)
                .ValueGeneratedOnAdd()
                .HasColumnName("CTDOITUONGNO");
            entity.Property(e => e.Ctdtcksaubh)
                .HasMaxLength(100)
                .ValueGeneratedOnAdd()
                .HasColumnName("CTDTCKSAUBH");
            entity.Property(e => e.Dai)
                .ValueGeneratedOnAdd()
                .HasDefaultValueSql("0")
                .HasColumnType("NUMBER(38,17)")
                .HasColumnName("DAI");
            entity.Property(e => e.Doanhthu)
                .ValueGeneratedOnAdd()
                .HasDefaultValueSql("0")
                .HasColumnType("NUMBER(38,17)")
                .HasColumnName("DOANHTHU");
            entity.Property(e => e.Dongia)
                .ValueGeneratedOnAdd()
                .HasDefaultValueSql("0")
                .HasColumnType("NUMBER(38,17)")
                .HasColumnName("DONGIA");
            entity.Property(e => e.Dongiabanbuon)
                .ValueGeneratedOnAdd()
                .HasDefaultValueSql("0")
                .HasColumnType("NUMBER(38,17)")
                .HasColumnName("DONGIABANBUON");
            entity.Property(e => e.Dongiabanle)
                .ValueGeneratedOnAdd()
                .HasDefaultValueSql("0")
                .HasColumnType("NUMBER(38,17)")
                .HasColumnName("DONGIABANLE");
            entity.Property(e => e.Dongiamuacu)
                .ValueGeneratedOnAdd()
                .HasDefaultValueSql("0")
                .HasColumnType("NUMBER(38,17)")
                .HasColumnName("DONGIAMUACU");
            entity.Property(e => e.Dongiathung)
                .ValueGeneratedOnAdd()
                .HasColumnType("NUMBER(38,17)")
                .HasColumnName("DONGIATHUNG");
            entity.Property(e => e.Donvile)
                .HasMaxLength(20)
                .IsUnicode(false)
                .ValueGeneratedOnAdd()
                .HasColumnName("DONVILE");
            entity.Property(e => e.Donvilon)
                .HasMaxLength(30)
                .ValueGeneratedOnAdd()
                .HasColumnName("DONVILON");
            entity.Property(e => e.Donviquydoi)
                .ValueGeneratedOnAdd()
                .HasDefaultValueSql("0")
                .HasColumnType("NUMBER(38,17)")
                .HasColumnName("DONVIQUYDOI");
            entity.Property(e => e.Giatriconlai)
                .ValueGeneratedOnAdd()
                .HasColumnType("NUMBER(38,17)")
                .HasColumnName("GIATRICONLAI");
            entity.Property(e => e.Giatrihaomon)
                .ValueGeneratedOnAdd()
                .HasColumnType("NUMBER(38,17)")
                .HasColumnName("GIATRIHAOMON");
            entity.Property(e => e.Giatrikh)
                .ValueGeneratedOnAdd()
                .HasColumnType("NUMBER(38,17)")
                .HasColumnName("GIATRIKH");
            entity.Property(e => e.Giatrikhthang)
                .ValueGeneratedOnAdd()
                .HasColumnType("NUMBER(38,17)")
                .HasColumnName("GIATRIKHTHANG");
            entity.Property(e => e.Giatrinltaiche)
                .ValueGeneratedOnAdd()
                .HasDefaultValueSql("0")
                .HasColumnType("NUMBER(38,17)")
                .HasColumnName("GIATRINLTAICHE");
            entity.Property(e => e.Giavonhangban)
                .ValueGeneratedOnAdd()
                .HasDefaultValueSql("0")
                .HasColumnType("NUMBER(38,17)")
                .HasColumnName("GIAVONHANGBAN");
            entity.Property(e => e.Hangkhuyenmai)
                .HasPrecision(18)
                .ValueGeneratedOnAdd()
                .HasDefaultValueSql("0")
                .HasColumnName("HANGKHUYENMAI");
            entity.Property(e => e.Isxuatdichdanh)
                .ValueGeneratedOnAdd()
                .HasColumnType("NUMBER")
                .HasColumnName("ISXUATDICHDANH");
            entity.Property(e => e.Kyhieuhoadon)
                .HasMaxLength(100)
                .IsUnicode(false)
                .ValueGeneratedOnAdd()
                .HasColumnName("KYHIEUHOADON");
            entity.Property(e => e.Loaihangck)
                .HasMaxLength(500)
                .ValueGeneratedOnAdd()
                .HasColumnName("LOAIHANGCK");
            entity.Property(e => e.Loaiphanbocp)
                .HasMaxLength(20)
                .ValueGeneratedOnAdd()
                .HasColumnName("LOAIPHANBOCP");
            entity.Property(e => e.Mabo)
                .HasMaxLength(100)
                .ValueGeneratedOnAdd()
                .HasColumnName("MABO");
            entity.Property(e => e.Macongthuc)
                .HasMaxLength(50)
                .IsUnicode(false)
                .ValueGeneratedOnAdd()
                .HasColumnName("MACONGTHUC");
            entity.Property(e => e.Mactkm)
                .HasMaxLength(20)
                .IsUnicode(false)
                .ValueGeneratedOnAdd()
                .HasColumnName("MACTKM");
            entity.Property(e => e.Mactkmlink)
                .HasMaxLength(100)
                .ValueGeneratedOnAdd()
                .HasColumnName("MACTKMLINK");
            entity.Property(e => e.Madonvi)
                .HasMaxLength(20)
                .IsUnicode(false)
                .ValueGeneratedOnAdd()
                .HasColumnName("MADONVI");
            entity.Property(e => e.Madonvinhap)
                .HasMaxLength(100)
                .ValueGeneratedOnAdd()
                .HasColumnName("MADONVINHAP");
            entity.Property(e => e.Madvtinh)
                .HasMaxLength(20)
                .IsUnicode(false)
                .ValueGeneratedOnAdd()
                .HasColumnName("MADVTINH");
            entity.Property(e => e.Mahangbankm)
                .HasMaxLength(50)
                .ValueGeneratedOnAdd()
                .HasColumnName("MAHANGBANKM");
            entity.Property(e => e.Makehang)
                .HasMaxLength(10)
                .ValueGeneratedOnAdd()
                .HasColumnName("MAKEHANG");
            entity.Property(e => e.Makh)
                .HasMaxLength(50)
                .ValueGeneratedOnAdd()
                .HasColumnName("MAKH");
            entity.Property(e => e.Makhonhap)
                .HasMaxLength(20)
                .IsUnicode(false)
                .ValueGeneratedOnAdd()
                .HasColumnName("MAKHONHAP");
            entity.Property(e => e.Makhoxuat)
                .HasMaxLength(20)
                .IsUnicode(false)
                .ValueGeneratedOnAdd()
                .HasColumnName("MAKHOXUAT");
            entity.Property(e => e.Maloaivtu)
                .HasMaxLength(50)
                .ValueGeneratedOnAdd()
                .HasColumnName("MALOAIVTU");
            entity.Property(e => e.Mangoaite)
                .HasMaxLength(20)
                .IsUnicode(false)
                .ValueGeneratedOnAdd()
                .HasColumnName("MANGOAITE");
            entity.Property(e => e.Manhomvtu)
                .HasMaxLength(50)
                .ValueGeneratedOnAdd()
                .HasColumnName("MANHOMVTU");
            entity.Property(e => e.Mathanhpham)
                .HasMaxLength(50)
                .IsUnicode(false)
                .ValueGeneratedOnAdd()
                .HasColumnName("MATHANHPHAM");
            entity.Property(e => e.Matkchiphico)
                .HasMaxLength(20)
                .IsUnicode(false)
                .ValueGeneratedOnAdd()
                .HasColumnName("MATKCHIPHICO");
            entity.Property(e => e.Matkchiphikhacco)
                .HasMaxLength(20)
                .IsUnicode(false)
                .ValueGeneratedOnAdd()
                .HasColumnName("MATKCHIPHIKHACCO");
            entity.Property(e => e.Matkchiphikhacno)
                .HasMaxLength(20)
                .IsUnicode(false)
                .ValueGeneratedOnAdd()
                .HasColumnName("MATKCHIPHIKHACNO");
            entity.Property(e => e.Matkchiphino)
                .HasMaxLength(20)
                .IsUnicode(false)
                .ValueGeneratedOnAdd()
                .HasColumnName("MATKCHIPHINO");
            entity.Property(e => e.Matkckco)
                .HasMaxLength(20)
                .ValueGeneratedOnAdd()
                .HasColumnName("MATKCKCO");
            entity.Property(e => e.Matkckno)
                .HasMaxLength(20)
                .ValueGeneratedOnAdd()
                .HasColumnName("MATKCKNO");
            entity.Property(e => e.Matkcksaubhco)
                .HasMaxLength(50)
                .ValueGeneratedOnAdd()
                .HasColumnName("MATKCKSAUBHCO");
            entity.Property(e => e.Matkcksaubhno)
                .HasMaxLength(50)
                .ValueGeneratedOnAdd()
                .HasColumnName("MATKCKSAUBHNO");
            entity.Property(e => e.Matkco)
                .HasMaxLength(20)
                .ValueGeneratedOnAdd()
                .HasColumnName("MATKCO");
            entity.Property(e => e.Matkcokm)
                .HasMaxLength(20)
                .IsUnicode(false)
                .ValueGeneratedOnAdd()
                .HasColumnName("MATKCOKM");
            entity.Property(e => e.Matkcovatkhautru)
                .HasMaxLength(20)
                .ValueGeneratedOnAdd()
                .HasColumnName("MATKCOVATKHAUTRU");
            entity.Property(e => e.Matkggiaco)
                .HasMaxLength(20)
                .ValueGeneratedOnAdd()
                .HasColumnName("MATKGGIACO");
            entity.Property(e => e.Matkggiano)
                .HasMaxLength(20)
                .ValueGeneratedOnAdd()
                .HasColumnName("MATKGGIANO");
            entity.Property(e => e.Matkgiavon)
                .HasMaxLength(20)
                .IsUnicode(false)
                .ValueGeneratedOnAdd()
                .HasColumnName("MATKGIAVON");
            entity.Property(e => e.Matklephico)
                .HasMaxLength(20)
                .IsUnicode(false)
                .ValueGeneratedOnAdd()
                .HasColumnName("MATKLEPHICO");
            entity.Property(e => e.Matklephino)
                .HasMaxLength(20)
                .IsUnicode(false)
                .ValueGeneratedOnAdd()
                .HasColumnName("MATKLEPHINO");
            entity.Property(e => e.Matkno)
                .HasMaxLength(20)
                .ValueGeneratedOnAdd()
                .HasColumnName("MATKNO");
            entity.Property(e => e.Matknokm)
                .HasMaxLength(20)
                .IsUnicode(false)
                .ValueGeneratedOnAdd()
                .HasColumnName("MATKNOKM");
            entity.Property(e => e.Matknovatkhautru)
                .HasMaxLength(20)
                .ValueGeneratedOnAdd()
                .HasColumnName("MATKNOVATKHAUTRU");
            entity.Property(e => e.Matkthuedbco)
                .HasMaxLength(20)
                .ValueGeneratedOnAdd()
                .HasColumnName("MATKTHUEDBCO");
            entity.Property(e => e.Matkthuedbno)
                .HasMaxLength(20)
                .ValueGeneratedOnAdd()
                .HasColumnName("MATKTHUEDBNO");
            entity.Property(e => e.Matkthuenkco)
                .HasMaxLength(20)
                .ValueGeneratedOnAdd()
                .HasColumnName("MATKTHUENKCO");
            entity.Property(e => e.Matkthuenkno)
                .HasMaxLength(20)
                .ValueGeneratedOnAdd()
                .HasColumnName("MATKTHUENKNO");
            entity.Property(e => e.Matkthuevatco)
                .HasMaxLength(20)
                .ValueGeneratedOnAdd()
                .HasColumnName("MATKTHUEVATCO");
            entity.Property(e => e.Matkthuevatno)
                .HasMaxLength(20)
                .ValueGeneratedOnAdd()
                .HasColumnName("MATKTHUEVATNO");
            entity.Property(e => e.Matkvtu)
                .HasMaxLength(20)
                .IsUnicode(false)
                .ValueGeneratedOnAdd()
                .HasColumnName("MATKVTU");
            entity.Property(e => e.Mavat)
                .HasMaxLength(20)
                .ValueGeneratedOnAdd()
                .HasColumnName("MAVAT");
            entity.Property(e => e.Mavatdb)
                .HasMaxLength(20)
                .ValueGeneratedOnAdd()
                .HasColumnName("MAVATDB");
            entity.Property(e => e.Mavatnk)
                .HasMaxLength(20)
                .IsUnicode(false)
                .ValueGeneratedOnAdd()
                .HasColumnName("MAVATNK");
            entity.Property(e => e.Mavattugdpk)
                .HasMaxLength(50)
                .ValueGeneratedOnAdd()
                .HasColumnName("MAVATTUGDPK");
            entity.Property(e => e.Mavtu)
                .HasMaxLength(50)
                .ValueGeneratedOnAdd()
                .HasColumnName("MAVTU");
            entity.Property(e => e.Mavtuncc)
                .HasMaxLength(50)
                .ValueGeneratedOnAdd()
                .HasColumnName("MAVTUNCC");
            entity.Property(e => e.Mavtuphu)
                .HasMaxLength(50)
                .ValueGeneratedOnAdd()
                .HasColumnName("MAVTUPHU");
            entity.Property(e => e.Ngayhachtoan1)
                .ValueGeneratedOnAdd()
                .HasDefaultValueSql("SYSDATE")
                .HasColumnType("DATE")
                .HasColumnName("NGAYHACHTOAN1");
            entity.Property(e => e.Ngayhoadon)
                .ValueGeneratedOnAdd()
                .HasColumnType("DATE")
                .HasColumnName("NGAYHOADON");
            entity.Property(e => e.Ngaytao)
                .HasMaxLength(20)
                .IsUnicode(false)
                .ValueGeneratedOnAdd()
                .HasColumnName("NGAYTAO");
            entity.Property(e => e.Nguyengia)
                .ValueGeneratedOnAdd()
                .HasDefaultValueSql("0")
                .HasColumnType("NUMBER(38,17)")
                .HasColumnName("NGUYENGIA");
            entity.Property(e => e.Noidung)
                .HasMaxLength(1000)
                .ValueGeneratedOnAdd()
                .HasColumnName("NOIDUNG");
            entity.Property(e => e.Rong)
                .ValueGeneratedOnAdd()
                .HasDefaultValueSql("0")
                .HasColumnType("NUMBER(38,17)")
                .HasColumnName("RONG");
            entity.Property(e => e.Sohoadon)
                .HasMaxLength(100)
                .IsUnicode(false)
                .ValueGeneratedOnAdd()
                .HasColumnName("SOHOADON");
            entity.Property(e => e.Soluong)
                .ValueGeneratedOnAdd()
                .HasDefaultValueSql("0")
                .HasColumnType("NUMBER(38,17)")
                .HasColumnName("SOLUONG");
            entity.Property(e => e.Soluongam)
                .ValueGeneratedOnAdd()
                .HasDefaultValueSql("0")
                .HasColumnType("NUMBER(38,17)")
                .HasColumnName("SOLUONGAM");
            entity.Property(e => e.Soluongthung)
                .HasMaxLength(50)
                .ValueGeneratedOnAdd()
                .HasColumnName("SOLUONGTHUNG");
            entity.Property(e => e.Soluongton)
                .ValueGeneratedOnAdd()
                .HasDefaultValueSql("0")
                .HasColumnType("NUMBER(38,17)")
                .HasColumnName("SOLUONGTON");
            entity.Property(e => e.Sort)
                .ValueGeneratedOnAdd()
                .HasColumnType("NUMBER")
                .HasColumnName("SORT");
            entity.Property(e => e.Tenvtu)
                .HasMaxLength(1000)
                .ValueGeneratedOnAdd()
                .HasColumnName("TENVTU");
            entity.Property(e => e.Thanhtien)
                .ValueGeneratedOnAdd()
                .HasDefaultValueSql("0")
                .HasColumnType("NUMBER(38,17)")
                .HasColumnName("THANHTIEN");
            entity.Property(e => e.Thoigiansd)
                .ValueGeneratedOnAdd()
                .HasColumnType("DATE")
                .HasColumnName("THOIGIANSD");
            entity.Property(e => e.Tienchiphi)
                .ValueGeneratedOnAdd()
                .HasDefaultValueSql("0")
                .HasColumnType("NUMBER(38,17)")
                .HasColumnName("TIENCHIPHI");
            entity.Property(e => e.Tienchiphikhac)
                .ValueGeneratedOnAdd()
                .HasDefaultValueSql("0")
                .HasColumnType("NUMBER(38,17)")
                .HasColumnName("TIENCHIPHIKHAC");
            entity.Property(e => e.Tienck)
                .ValueGeneratedOnAdd()
                .HasDefaultValueSql("0")
                .HasColumnType("NUMBER(38,17)")
                .HasColumnName("TIENCK");
            entity.Property(e => e.Tiencksaubh)
                .ValueGeneratedOnAdd()
                .HasDefaultValueSql("0")
                .HasColumnType("NUMBER(38,17)")
                .HasColumnName("TIENCKSAUBH");
            entity.Property(e => e.Tienggia)
                .ValueGeneratedOnAdd()
                .HasDefaultValueSql("0")
                .HasColumnType("NUMBER(38,17)")
                .HasColumnName("TIENGGIA");
            entity.Property(e => e.Tienhang)
                .ValueGeneratedOnAdd()
                .HasDefaultValueSql("0")
                .HasColumnType("NUMBER(38,17)")
                .HasColumnName("TIENHANG");
            entity.Property(e => e.Tienkm)
                .ValueGeneratedOnAdd()
                .HasDefaultValueSql("0")
                .HasColumnType("NUMBER(38,17)")
                .HasColumnName("TIENKM");
            entity.Property(e => e.Tienlephi)
                .ValueGeneratedOnAdd()
                .HasDefaultValueSql("0")
                .HasColumnType("NUMBER(38,17)")
                .HasColumnName("TIENLEPHI");
            entity.Property(e => e.Tienvat)
                .ValueGeneratedOnAdd()
                .HasDefaultValueSql("0")
                .HasColumnType("NUMBER(38,17)")
                .HasColumnName("TIENVAT");
            entity.Property(e => e.Tienvatdb)
                .ValueGeneratedOnAdd()
                .HasDefaultValueSql("0")
                .HasColumnType("NUMBER(38,17)")
                .HasColumnName("TIENVATDB");
            entity.Property(e => e.Tienvatkhautru)
                .ValueGeneratedOnAdd()
                .HasDefaultValueSql("0")
                .HasColumnType("NUMBER(38,17)")
                .HasColumnName("TIENVATKHAUTRU");
            entity.Property(e => e.Tienvatnk)
                .ValueGeneratedOnAdd()
                .HasDefaultValueSql("0")
                .HasColumnType("NUMBER(38,17)")
                .HasColumnName("TIENVATNK");
            entity.Property(e => e.Tilelaibanbuon)
                .ValueGeneratedOnAdd()
                .HasDefaultValueSql("0")
                .HasColumnType("NUMBER(38,17)")
                .HasColumnName("TILELAIBANBUON");
            entity.Property(e => e.Tilelaibanle)
                .ValueGeneratedOnAdd()
                .HasDefaultValueSql("0")
                .HasColumnType("NUMBER(38,17)")
                .HasColumnName("TILELAIBANLE");
            entity.Property(e => e.Tilelaibanletheogiaban)
                .ValueGeneratedOnAdd()
                .HasColumnType("NUMBER(38,17)")
                .HasColumnName("TILELAIBANLETHEOGIABAN");
            entity.Property(e => e.Tiletanggia)
                .HasDefaultValueSql("0")
                .HasColumnType("NUMBER")
                .HasColumnName("TILETANGGIA");
            entity.Property(e => e.Trangthaitruton)
                .ValueGeneratedOnAdd()
                .HasDefaultValueSql("1")
                .HasColumnType("NUMBER(38,17)")
                .HasColumnName("TRANGTHAITRUTON");
            entity.Property(e => e.Trongluong)
                .ValueGeneratedOnAdd()
                .HasDefaultValueSql("0")
                .HasColumnType("NUMBER(38,17)")
                .HasColumnName("TRONGLUONG");
            entity.Property(e => e.Trongluongthung)
                .HasDefaultValueSql("0")
                .HasColumnType("NUMBER(38,7)")
                .HasColumnName("TRONGLUONGTHUNG");
            entity.Property(e => e.Ttienlaibanbuon)
                .ValueGeneratedOnAdd()
                .HasDefaultValueSql("0")
                .HasColumnType("NUMBER(38,17)")
                .HasColumnName("TTIENLAIBANBUON");
            entity.Property(e => e.Ttienlaibanle)
                .ValueGeneratedOnAdd()
                .HasDefaultValueSql("0")
                .HasColumnType("NUMBER(38,17)")
                .HasColumnName("TTIENLAIBANLE");
            entity.Property(e => e.Tygia)
                .ValueGeneratedOnAdd()
                .HasDefaultValueSql("0")
                .HasColumnType("NUMBER(38,17)")
                .HasColumnName("TYGIA");
            entity.Property(e => e.Tyleck)
                .ValueGeneratedOnAdd()
                .HasDefaultValueSql("0")
                .HasColumnType("NUMBER(38,17)")
                .HasColumnName("TYLECK");
            entity.Property(e => e.Tylecksaubh)
                .ValueGeneratedOnAdd()
                .HasDefaultValueSql("0")
                .HasColumnType("NUMBER(38,17)")
                .HasColumnName("TYLECKSAUBH");
            entity.Property(e => e.Tyleggia)
                .ValueGeneratedOnAdd()
                .HasDefaultValueSql("0")
                .HasColumnType("NUMBER(38,17)")
                .HasColumnName("TYLEGGIA");
            entity.Property(e => e.Tylekhthang)
                .ValueGeneratedOnAdd()
                .HasColumnType("NUMBER(38,17)")
                .HasColumnName("TYLEKHTHANG");
            entity.Property(e => e.Tylekm)
                .ValueGeneratedOnAdd()
                .HasDefaultValueSql("0")
                .HasColumnType("NUMBER(38,17)")
                .HasColumnName("TYLEKM");
            entity.Property(e => e.Vat)
                .ValueGeneratedOnAdd()
                .HasDefaultValueSql("0")
                .HasColumnType("NUMBER(38,17)")
                .HasColumnName("VAT");
            entity.Property(e => e.Vatdb)
                .ValueGeneratedOnAdd()
                .HasDefaultValueSql("0")
                .HasColumnType("NUMBER(38,17)")
                .HasColumnName("VATDB");
            entity.Property(e => e.Vatnk)
                .ValueGeneratedOnAdd()
                .HasDefaultValueSql("0")
                .HasColumnType("NUMBER(38,17)")
                .HasColumnName("VATNK");
        });
        modelBuilder.HasSequence("BTKCCT_SEQ");
        modelBuilder.HasSequence("CTKTGTGTDVAO_SEQ");
        modelBuilder.HasSequence("CTUGOC_KEY_SEQ");
        modelBuilder.HasSequence("DM_TEST3_ID_SEQ");
        modelBuilder.HasSequence("DMGIADV_SEQ");
        modelBuilder.HasSequence("DMLOAIHOPDONG_SEQ");
        modelBuilder.HasSequence("DMLOAILAODONG_SEQ");
        modelBuilder.HasSequence("DMNHOMVITRI_SEQ");
        modelBuilder.HasSequence("DMVANBANXULY_SEQ");
        modelBuilder.HasSequence("DMVITRI_SEQ");
        modelBuilder.HasSequence("LICHDATNCC_SEQ");
        modelBuilder.HasSequence("SLBANLE_SEQ");
        modelBuilder.HasSequence("TRUNGBAYGDCHITIET_SEQ");
        modelBuilder.HasSequence("VATTUGDCT_SEQ");
        modelBuilder.HasSequence("VATTUGDDHCT_SEQ");

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}

﻿using System;
using System.Collections.Generic;

namespace AccountingDP.EF.Models.VietseaModel;

public partial class Vattugdct
{
    public string Mavattugdpk { get; set; } = null!;

    public string Madonvi { get; set; } = null!;

    public string Mavtu { get; set; } = null!;

    public decimal? Soluong { get; set; }

    public string? Mangoaite { get; set; }

    public decimal? Tygia { get; set; }

    public decimal? Nguyengia { get; set; }

    public decimal? Dongia { get; set; }

    public decimal? Tienhang { get; set; }

    public string? Mavatnk { get; set; }

    public decimal? Vatnk { get; set; }

    public decimal? Tienvatnk { get; set; }

    public string? Mavat { get; set; }

    public decimal? Vat { get; set; }

    public decimal? Tienvat { get; set; }

    public string? Mavatdb { get; set; }

    public decimal? Vatdb { get; set; }

    public decimal? Tienvatdb { get; set; }

    public decimal? Sort { get; set; }

    public decimal? Tyleck { get; set; }

    public decimal? Tienck { get; set; }

    public decimal? Tyleggia { get; set; }

    public decimal? Tienggia { get; set; }

    public string? Loaiphanbocp { get; set; }

    public decimal? Tienlephi { get; set; }

    public decimal? Tienchiphi { get; set; }

    public string? Matkno { get; set; }

    public string? Matkco { get; set; }

    public string? Matkthuenkno { get; set; }

    public string? Matkthuenkco { get; set; }

    public string? Matkthuedbno { get; set; }

    public string? Matkthuedbco { get; set; }

    public string? Matkthuevatno { get; set; }

    public string? Matkthuevatco { get; set; }

    public string? Matkckno { get; set; }

    public string? Matkckco { get; set; }

    public string? Matkggiano { get; set; }

    public string? Matkggiaco { get; set; }

    public string? Madvtinh { get; set; }

    public string? Kyhieuhoadon { get; set; }

    public DateTime? Ngayhoadon { get; set; }

    public string? Sohoadon { get; set; }

    public string? Ngaytao { get; set; }

    public string? Matklephino { get; set; }

    public string? Matklephico { get; set; }

    public string? Matkchiphino { get; set; }

    public string? Matkchiphico { get; set; }

    public string? Matkchiphikhacno { get; set; }

    public string? Matkchiphikhacco { get; set; }

    public decimal? Tienchiphikhac { get; set; }

    public string? Makhonhap { get; set; }

    public string? Makhoxuat { get; set; }

    public string? Maloaivtu { get; set; }

    public string? Manhomvtu { get; set; }

    public decimal? Thanhtien { get; set; }

    public decimal? Giavonhangban { get; set; }

    public string? Matkgiavon { get; set; }

    public string? Matkvtu { get; set; }

    public string? Tenvtu { get; set; }

    public string? Macongthuc { get; set; }

    public string? Mathanhpham { get; set; }

    public decimal Itemid { get; set; }

    public decimal? Isxuatdichdanh { get; set; }

    public decimal? Giatrinltaiche { get; set; }

    public string? Donvile { get; set; }

    public decimal? Soluongton { get; set; }

    public string? Matknokm { get; set; }

    public string? Matkcokm { get; set; }

    public decimal? Tylekm { get; set; }

    public decimal? Tienkm { get; set; }

    public string? Mactkm { get; set; }

    public string? Makh { get; set; }

    public decimal? Doanhthu { get; set; }

    public decimal? Tienvatkhautru { get; set; }

    public string? Matknovatkhautru { get; set; }

    public string? Matkcovatkhautru { get; set; }

    public DateTime? Ngayhachtoan1 { get; set; }

    public string? Mabo { get; set; }

    public string? Mahangbankm { get; set; }

    public string? Donvilon { get; set; }

    public decimal? Donviquydoi { get; set; }

    public string? Ctdoituong { get; set; }

    public long? Hangkhuyenmai { get; set; }

    public string? Ctdoituongkm { get; set; }

    public string? Ctdoituongcp { get; set; }

    public string? Mavtuncc { get; set; }

    public string? Barcodevtu { get; set; }

    public string? Mavtuphu { get; set; }

    public decimal? Dongiabanle { get; set; }

    public decimal? Tilelaibanle { get; set; }

    public decimal? Dongiabanbuon { get; set; }

    public decimal? Tilelaibanbuon { get; set; }

    public decimal? Ttienlaibanle { get; set; }

    public decimal? Ttienlaibanbuon { get; set; }

    public decimal? Dongiamuacu { get; set; }

    public string? Soluongthung { get; set; }

    public decimal? Dongiathung { get; set; }

    public string? Mactkmlink { get; set; }

    public decimal? Tylecksaubh { get; set; }

    public decimal? Tiencksaubh { get; set; }

    public string? Ctdtcksaubh { get; set; }

    public string? Matkcksaubhno { get; set; }

    public string? Matkcksaubhco { get; set; }

    public decimal? Trongluong { get; set; }

    public decimal? Dai { get; set; }

    public decimal? Rong { get; set; }

    public decimal? Cao { get; set; }

    public decimal? Giatrikh { get; set; }

    public decimal? Tylekhthang { get; set; }

    public decimal? Giatrikhthang { get; set; }

    public decimal? Giatriconlai { get; set; }

    public decimal? Giatrihaomon { get; set; }

    public DateTime? Thoigiansd { get; set; }

    public decimal? Trangthaitruton { get; set; }

    public decimal? Soluongam { get; set; }

    public string? Ctdoituongno { get; set; }

    public string? Ctdoituongco { get; set; }

    public string? Madonvinhap { get; set; }

    public string? Noidung { get; set; }

    public string? Loaihangck { get; set; }

    public decimal? Tilelaibanletheogiaban { get; set; }

    public string? Makehang { get; set; }

    public decimal? Tiletanggia { get; set; }

    public decimal? Trongluongthung { get; set; }
}

﻿using AccountingDP.EF.Models.ThanhCongModel;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using OfficeOpenXml;
using System;
using System.Windows;

namespace AccountingDP.ViewModels
{
    public partial class MainViewModel : ObservableObject
    { 
        public VietseaViewModel VietseaViewModel { get; }

        [ObservableProperty]
        private AccountingContext accountingContext = new();
       
        public MainViewModel()
        {
            ExcelPackage.LicenseContext = LicenseContext.Commercial;
            VietseaViewModel = new();
        }

        [RelayCommand]
        public void TestEF()
        {
            object gg = Services.VietseaService.getNXT(DateTime.Now);
            string a = "";

            MessageBox.Show("test successfully");
        }
    }
}

﻿using AccountingDP.Models;
using AccountingDP.Services;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using Microsoft.Win32;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Threading.Tasks;
using System.Windows;

namespace AccountingDP.ViewModels
{
    public partial class VietseaViewModel : ObservableObject
    {
        public VietseaViewModel()
        {
           
        }

        [ObservableProperty]
        private DateTime startDate = DateTime.Now;

        [ObservableProperty]
        private DateTime endDate = DateTime.Now;

        [ObservableProperty]
        private ObservableCollection<XNT> xNTCollection= new();

        [ObservableProperty]
        private XNT selectedXNT;

        [RelayCommand]
        public async Task LoadXNTAsync()
        {
            XNTCollection.Clear();
            XNTCollection = await VietseaService.getXNTCoreAsync(EndDate);
        }

        [RelayCommand]
        public async Task GetXNTAsync()
        {
            List<Models.XNTDto> XNTData = await VietseaService.getNXT(EndDate);

            using ExcelPackage excelPackage = new ExcelPackage();
            ExcelWorksheet excelWorksheet = excelPackage.Workbook.Worksheets.Add("XNT");

            int rowIndex = 1;
            int colIndex = 1;

            WriteData(XNTData, excelWorksheet, ref rowIndex, ref colIndex);

            SaveFileDialog saveFileDialog = new()
            {
                Filter = "Excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*",
                FileName = $"XNT_{EndDate:dd-MM-yyyy}.xlsx"
            };
            if (saveFileDialog.ShowDialog() != true)
            {
                return;
            }

            string fileName = saveFileDialog.FileName;
            FileInfo newFile = new FileInfo(fileName);
            await excelPackage.SaveAsAsync(newFile);
            MessageBox.Show("Kết xuất file thành công");
        }

        private void WriteData(List<XNTDto> result, ExcelWorksheet excelWorksheet, ref int rowIndex, ref int colIndex)
        {
            //header
            excelWorksheet.Cells[rowIndex, colIndex++].Value = nameof(Models.XNTDto.Makhohang);
            excelWorksheet.Cells[rowIndex, colIndex++].Value = nameof(Models.XNTDto.Mavtu);
            excelWorksheet.Cells[rowIndex, colIndex++].Value = nameof(Models.XNTDto.Tenvtu);
            excelWorksheet.Cells[rowIndex, colIndex++].Value = nameof(Models.XNTDto.Donvile);
            excelWorksheet.Cells[rowIndex, colIndex++].Value = nameof(Models.XNTDto.Toncuoikysl);
            excelWorksheet.Cells[rowIndex, colIndex++].Value = nameof(Models.XNTDto.GiaBanChuaVat);

            foreach (Models.XNTDto xNTDto in result)
            {
                colIndex = 1;
                rowIndex++;
                excelWorksheet.Cells[rowIndex, colIndex++].Value = xNTDto.Makhohang;
                excelWorksheet.Cells[rowIndex, colIndex++].Value = xNTDto.Mavtu;
                excelWorksheet.Cells[rowIndex, colIndex++].Value = xNTDto.Tenvtu;
                excelWorksheet.Cells[rowIndex, colIndex++].Value = xNTDto.Donvile;
                excelWorksheet.Cells[rowIndex, colIndex++].Value = xNTDto.Toncuoikysl;
                excelWorksheet.Cells[rowIndex, colIndex++].Value = xNTDto.GiaBanChuaVat;
            }

            excelWorksheet.Row(1).Style.Font.Bold = true;
            excelWorksheet.Column(6).Style.Numberformat.Format = "#,##0";
            excelWorksheet.Columns.AutoFit();
        }

        [RelayCommand]
        public async Task GetvattugdctAsync()
        {
            await VietseaService.GetVattugdctAsync();

        }

    }
}
